#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <errno.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <netdb.h>

#include <htmlgrab/htmlgrab.h>

int
html_connect(html_conn_t *htc)
{
    assert(htc);

    struct addrinfo hints, *servinfo;

    memset(&hints, 0, sizeof hints); // make sure the struct is empty
    hints.ai_family = AF_INET;     // don't care IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM; // TCP stream sockets

    int status = getaddrinfo(htc->server, htc->port, &hints, &servinfo);
    if (status != 0) {
        printf("getaddrinfo error: %d\n", status);
        perror(htc->server);
        return -1;
    }

    htc->socket = socket(servinfo->ai_family, servinfo->ai_socktype, servinfo->ai_protocol);
    if (htc->socket < 0) {
        perror("socket");
        return -1;
    }

    status = connect(htc->socket, servinfo->ai_addr, servinfo->ai_addrlen);
    if (status != 0) {
        perror("connect");
        return -1;
    }

    return 0;
}

