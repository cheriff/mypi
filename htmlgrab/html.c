#include <string.h>
#include <assert.h>
#include <stdio.h>

#include <htmlgrab/htmlgrab.h>

int
html_conn_init(html_conn_t *htc)
{
    assert(htc);
    memset(htc, 0, sizeof(*htc));

    // Now set sensible defaults
    strcpy(htc->port, "80");
    htc->accept = MIME_NONE;
    htc->bufflen = HTC_BUFFLEN;
    htc->request = HG_GET;
    htc->resource = "/";

    return 0;
}

int
html_conn_set_server(html_conn_t *htc, const char *server)
{
    assert(htc);
    htc->server = server;
    return 0;
}

int
html_conn_set_port(html_conn_t *htc, uint16_t port)
{
    assert(htc);
    snprintf(htc->port,6, "%d", port);
    return 0;
}

int
html_conn_add_accept(html_conn_t *htc, mime_t accept)
{
    assert(htc);
    htc->accept = accept;
    return 0;
}

int
html_conn_set_resource(html_conn_t *htc, const char * resource)
{
    assert(htc);
    htc->resource = resource;
    return 0;
}

int
html_conn_set_rcv_func(html_conn_t *htc, http_rcv_func func)
{
    assert(htc);
    htc->func = func;
    return 0;
}

int
html_conn_set_userdata(html_conn_t *htc, void *user)
{
    assert(htc);
    htc->user = user;
    return 0;
}
void *
html_conn_userdata(html_conn_t *htc)
{
    assert(htc);
    return htc->user;
}
