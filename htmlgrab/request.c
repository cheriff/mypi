#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <errno.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <netdb.h>

#include <htmlgrab/htmlgrab.h>

int
html_send_requect(html_conn_t *htc)
{
    assert(htc);

    char *req[] = { "GET", "POST" };
    char * header;
    int status = asprintf(&header, "%s %s HTTP/1.0\n",
        req[htc->request],
        htc->resource);

    int wrote = send(htc->socket, header, status, 0);
    if (wrote != status) {
        printf("Unhandled: Short write\n");
        return -1;
    }

    if (htc->accept != MIME_NONE) {
        printf("FIXME: DO SOMETHING\n");
    }

    wrote = send(htc->socket, "\n", 1, 0);
    if (wrote != 1) {
        printf("Unhandled: Short write\n");
        return -1;
    }

    return 0;
}
