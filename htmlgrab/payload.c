#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <errno.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <netdb.h>

#include <htmlgrab/htmlgrab.h>

static int
message_complete_cb(http_parser*p)
{
    return 0;
}

static int
body_cb(http_parser*p, const char *at, size_t length)
{
    assert(p);
    html_conn_t *htc = p->data;
    assert(htc);

    if (!htc->func) {
        printf("html_recv_payload called with no callback set\n");
        return -1;
    }

    return htc->func(htc, at, length);
}

int
html_recv_payload(html_conn_t *htc)
{
    assert(htc);

    http_parser *parser = &htc->parser;
    http_parser_settings *settings = &htc->settings;

    settings->on_body = body_cb;
    settings->on_message_complete = message_complete_cb;

    /* Drain any accumulated body that may have been gathered while recieveing
     * the headers
     */
    if (htc->partial_len != 0) {
        int r = htc->func(htc, htc->partial_base, htc->partial_len);
        htc->partial_len = 0;
        htc->partial_base = NULL;
        if (r !=0) return r;
    }

    while (1) {
        int got = recv(htc->socket, htc->buff, htc->bufflen, 0);
        if (got == 0) {
            break;
        }
        if (got < 0) {
            perror("recv");
            break;
        };

        int parsed = http_parser_execute(parser, settings, htc->buff, got);
        assert(parsed >= 0);

        if (parsed == 0) {
            printf("Warning: data downloaded, not parsed\n");
            return 0;
        }
    }

    return 0;
}
