#include <string.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>

#include <errno.h>
#include <sys/socket.h>
#include <resolv.h>
#include <arpa/inet.h>

#include <sys/types.h>
#include <netdb.h>

#include <htmlgrab/htmlgrab.h>


//#define HTTP_DEBUG 1


#if HTTP_DEBUG
static int message_begin_cb(http_parser*p) { return !printf("CB: MessageBegin\n"); }
static int message_complete_cb(http_parser*p) { return !printf("CB: MessageComplete\n"); }

static int url_cb(http_parser*p, const char *at, size_t length) { printf("CB: URL: '"); for (int i=0; i<length; i++) printf("%c", at[i]); return !printf("'\n");}
static int status_cb(http_parser*p, const char *at, size_t length) { printf("CB: STATUS: '"); for (int i=0; i<length; i++) printf("%c", at[i]); return !printf("'\n");}
static int header_f_cb(http_parser*p, const char *at, size_t length) { printf("CB: HEADER_F: '"); for (int i=0; i<length; i++) printf("%c", at[i]); return !printf("'\n");}
static int heaer_v_cb(http_parser*p, const char *at, size_t length) { printf("CB: HEADER_V: '"); for (int i=0; i<length; i++) printf("%c", at[i]); return !printf("'\n");}
#endif

static int
body_cb(http_parser*p, const char *at, size_t length)
{
#if HTTP_DEBUG
    printf("CB: BODY: '");
    for (int i=0; i<length; i++)
        printf("%c", at[i]);
    printf("'\n");
#endif

    /* If there's any residual payload in this parsed section, make a note of it
     * so that the first call to html_recv_payload() can gather it up
     */
    
    html_conn_t *htc = p->data;

    assert(htc);
    assert(htc->header_complete);

    assert (at >= htc->buff);
    assert (at < htc->buff + htc->bufflen);

    htc->partial_len = length;
    htc->partial_base = (char*)at;
    return 0;
}

static int
headers_complete_cb(http_parser*p)
{
#if HTTP_DEBUG
    printf("CB: HeadersComplete: Heaer: Body: %d, code %d\n",p->content_length, p->status_code);
#endif

    if (p->status_code < 200 || p->status_code > 299) {
        printf("Non-OK HTTP Code: %d\n", p->status_code);
        return -1;
    }
    if (p->status_code != 200) {
        printf("Warning: unexpected, but still OK (2xx) error code: %d\n", p->status_code);
    }

    html_conn_t *htc = p->data;
    htc->header_complete = 1;
    return 0;
}


int
html_recv_response(html_conn_t *htc)
{
    http_parser *parser = &htc->parser;
    http_parser_settings *settings = &htc->settings;

#if HTTP_DEBUG
    settings->on_message_begin = message_begin_cb;
    settings->on_message_complete = message_complete_cb;

    settings->on_url = url_cb;
    settings->on_status = status_cb;
    settings->on_header_field = header_f_cb;
    settings->on_header_value = heaer_v_cb;
#endif

    settings->on_headers_complete = headers_complete_cb;
    settings->on_body = body_cb;

    http_parser_init(parser, HTTP_RESPONSE);
    assert(parser->http_errno == HPE_OK);

    parser->data = htc;

    while (1) {
        int got = recv(htc->socket, htc->buff, htc->bufflen, 0);
        if (got < 0) {
            perror("recv");
            break;
        };

        int parsed = http_parser_execute(parser, settings, htc->buff, got);
        assert(parsed > 0);

        if (htc->header_complete) break;
    }

    return 0;
}
