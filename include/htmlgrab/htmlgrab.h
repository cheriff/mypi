#include <stdint.h>

#include <htmlgrab/http_parser.h>


typedef enum {
    MIME_NONE = 0,
    MIME_APPLIATION_JSON,
    MIME_TEST_JAVASCRIPT,
} mime_t;

typedef enum {
    HG_GET,
    HG_POST,
} http_request_t;

typedef enum {
    STATE_NEW = 0,
    STATE_CONNECTED,
    STATE_REQUEST_SENT,
    STATE_HEADER_RECEIVED,
    STATE_GETTING_BODY,
} conn_state_t;


struct html_conn;
typedef struct html_conn html_conn_t;

typedef int (*http_rcv_func) (html_conn_t *htc, const char *data, size_t len);

/* Just because you can see the struct members, please
 * don't set them directly!
 * Use the setters below, they could be doing extra
 * important stuff.
 */
struct html_conn {
    const char *server;
    char port[6];
    mime_t accept;
    const char *resource;
    http_request_t request;
    conn_state_t state;
    void *servinfo;
    int socket;

    http_rcv_func func;
    void *user;

#define HTC_BUFFLEN 1024
    http_parser parser;
    http_parser_settings settings;
    char buff[HTC_BUFFLEN];
    size_t bufflen;
    int header_complete;;
    int partial_len;
    char * partial_base;
};


int html_conn_init(html_conn_t *htc);
int html_conn_set_server(html_conn_t *htc, const char *server);
int html_conn_set_port(html_conn_t *htc, uint16_t port);
int html_conn_add_accept(html_conn_t *htc, mime_t accept);
int html_conn_add_request(html_conn_t *htc, http_request_t request);
int html_conn_set_resource(html_conn_t *htc, const char * resource);
int html_conn_set_rcv_func(html_conn_t *htc, http_rcv_func func);
int html_conn_set_userdata(html_conn_t *htc, void *user);
void * html_conn_userdata(html_conn_t *htc);

int html_connect(html_conn_t *htc);
int html_send_requect(html_conn_t *htc);
int html_recv_response(html_conn_t *htc);

int html_recv_payload(html_conn_t *htc);
