#!/usr/bin/env python

import sys
from pymake import *

try:
    from pymake import *
except ImportError as e:
    print "Unable to import from pymake. Did you forget to set PYTHONPATH"
    print
    print e
    sys.exit(0)


def lib_helper(src_dir, libname):
    lib_cfiles = findFiles(src_dir)
    lib_ofiles = [make_compile(c) for c in lib_cfiles]
    return make_lib(lib_ofiles, make_file(libname))


def make_app(src_dir, appname, libs=[]):
    app_cfiles = findFiles(src_dir)
    app_ofiles = [make_compile(c) for c in app_cfiles]
    return make_link(app_ofiles + libs, make_file(appname))


libs = []

with Toolchain().cflag("-Ijansson/src").cflag("-Iinclude/jansson"):
    libs.append(lib_helper("jansson/src/*.c", "lib/libjansson.a"))

with Toolchain().cflag("-Iinclude"):
    html_sources = ['htmlgrab/*.c', 'htmlgrab/http-parser/http_parser.c']
    libs.append(lib_helper(html_sources, "lib/libhtmlgrab.a"))

with Toolchain().cflag("-Iinclude"):
    app = make_app("src/*.c", "mypi", libs)

if 'clean' in sys.argv:
    errors = pymake_clean()
    print "Clean complete. {} errors".format(errors)
    sys.exit(0)


print "Available targets: ", pymake_list_targets()
pymake_write_graph("fabric")


