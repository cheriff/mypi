#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <pthread.h>

#include <jansson/jansson.h>
#include <htmlgrab/htmlgrab.h>

typedef struct {
    int len;
    int size;

    void *buffer;
    void *head, *tail;

    pthread_cond_t full_condition;
    pthread_cond_t empty_condition;
    pthread_mutex_t mutex;
    int complete;
} dlbuff_t;

#define IS_EMPTY(_b) ((_b)->head == (_b)->tail)
static inline int BUFF_USAGE(dlbuff_t *b)
{
    int space = b->tail - b->head;
    if (space < 0) space += b->size;
    return space;
}

static inline int BUFF_ROOM(dlbuff_t *b)
{
    return b->size - BUFF_USAGE(b)-1;
}

static int downloaded_data(html_conn_t *htc, const char *data, size_t len)
{
    static int c = 0;
    dlbuff_t *buff = html_conn_userdata(htc);

    if (buff->size >= len) {
        assert("Need bigger buffer");
    }

    pthread_mutex_lock(&buff->mutex);
    while(BUFF_ROOM(buff) < len) {
        // Too full, wait for consumer to drain some
        pthread_cond_wait(&buff->full_condition, &buff->mutex);
    }

    size_t orig = len;

    // FIXME: Be smarter and use memcpy in one or two chunks
    char *p = buff->tail;
    while(len) {
        *p = *data;
        p++; data++; len--;
        if (p == buff->buffer + buff->size) p = buff->buffer;
    }
    buff->tail = p;
    pthread_cond_signal(&buff->empty_condition);

    pthread_mutex_unlock(&buff->mutex);
    return 0;
}

void *dl_thread(void*thrd_data)
{
    html_conn_t *conn = (html_conn_t *)thrd_data;

    assert(html_connect(conn) == 0);
    assert(html_send_requect(conn) == 0);
    assert(html_recv_response(conn) == 0);
    assert(html_recv_payload(conn) == 0);

    dlbuff_t *buff = html_conn_userdata(conn);
    pthread_mutex_lock(&buff->mutex);
    buff->complete = 1;
    pthread_cond_signal(&buff->empty_condition);
    pthread_mutex_unlock(&buff->mutex);

    return NULL;
}


int test_buff_dl(html_conn_t *conn)
{
    int r;
    dlbuff_t buff;

    memset(&buff, 0, sizeof(buff));
    buff.size = 1024 * 1024;
    buff.buffer = malloc(buff.size);
    buff.head = buff.tail = buff.buffer;

    html_conn_set_rcv_func(conn, downloaded_data);
    html_conn_set_userdata(conn, &buff);

    r = pthread_cond_init(&buff.full_condition, NULL);
    assert(r==0);
    r = pthread_cond_init(&buff.empty_condition, NULL);
    assert(r==0);

    r = pthread_mutex_init(&buff.mutex, NULL);
    assert(r==0);

    FILE *fp = fopen("out.bin", "wb");
    assert(fp);

    pthread_t thread;
    r = pthread_create(&thread, NULL, dl_thread, conn);
    assert(r==0);

    pthread_mutex_lock(&buff.mutex);
    while (buff.complete == 0 || BUFF_USAGE(&buff)) {
        if (IS_EMPTY(&buff)) {
            pthread_cond_wait(&buff.empty_condition, &buff.mutex);
            continue;
        }

        int len = BUFF_USAGE(&buff);
        assert(len != 0);
        for (int i=0; i<len; i++) {
            assert(buff.head != buff.tail);
            fwrite(buff.head, 1, 1, fp);
            buff.head++;
            if (buff.head == buff.buffer+buff.size) buff.head = buff.buffer;
        }
    }
    fclose(fp);
    printf("DONE (%d) remaining!\n", BUFF_USAGE(&buff));
    pthread_mutex_unlock(&buff.mutex);


    r = pthread_join(thread, NULL);
    assert(r==0);

    r = pthread_cond_destroy(&buff.full_condition);
    assert(r==0);
    r = pthread_cond_destroy(&buff.empty_condition);
    assert(r==0);
    r = pthread_mutex_destroy(&buff.mutex);
    assert(r==0);


    return 0;

}
