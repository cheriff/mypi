#include <stdio.h>
#include <assert.h>

#include <jansson/jansson.h>
#include <htmlgrab/htmlgrab.h>

static char * type2str(json_type t) {
    switch(t){
        case JSON_OBJECT: return "JSON_OBJECT";
        case JSON_ARRAY: return "JSON_ARRAY";
        case JSON_STRING: return "JSON_STRING";
        case JSON_INTEGER: return "JSON_INTEGER";
        case JSON_REAL: return "JSON_REAL";
        case JSON_TRUE: return "JSON_TRUE";
        case JSON_FALSE: return "JSON_FALSE";
        case JSON_NULL: return "JSON_NULL";
        default:
                        return "UNKNOWN";
    }
}

static int printJson(json_t *j)
{
    switch(json_typeof(j)) {
        case JSON_OBJECT: return printf("{}");
        case JSON_ARRAY: return printf("[]");
        case JSON_STRING: return printf("'%s'", json_string_value(j));
        case JSON_INTEGER: return printf("%lldi", json_integer_value(j));
        case JSON_REAL: return printf("%ff", json_real_value(j));
        case JSON_TRUE: return printf("<True>");
        case JSON_FALSE: return printf("<False>");
        case JSON_NULL: return printf( "<NULL>");
        default:
                        return printf("<UNKNOWN>");
    }
}

static int
do_print(html_conn_t *htc, const char *data, size_t len)
{
    for(int i=0; i<len; i++) printf("%c", data[i]);
    return 0;
}

int main(int argc, char *argv[])
{

    char *text = "{ \"foo\":\"bar\", \"somenum\" : 42 }";

    json_t *root;
    json_error_t error;

    root = json_loads(text, 0, &error);
    if (root == NULL) {
        printf("Error at %d : %d\n", error.line, error.column);
        printf("\tSource: '%s'\n", error.source);
        printf("\tError: '%s'\n", error.text);
        return -1;
    }
    printf("Root is %p :: %s\n", root, type2str(json_typeof(root)));

    const char *key;
    json_t *value;
    json_object_foreach(root, key, value) {
        printf("HAVE: %s [%s] = ", key, type2str(json_typeof(value)));
        printJson(value); printf("\n");
    }

    html_conn_t conn;
    html_conn_init(&conn);
    html_conn_set_rcv_func(&conn, do_print);
    html_conn_set_server(&conn, "localhost");
    html_conn_set_port(&conn, 8000);
    html_conn_set_resource(&conn, "/GetVideoList");

    int test_buff_dl(html_conn_t *conn);
    // TEST THE DOWNLOAD BUFFER
    if (1) {
        test_buff_dl(&conn);
    } else {
        assert(html_connect(&conn) == 0);
        assert(html_send_requect(&conn) == 0);
        assert(html_recv_response(&conn) == 0);
        assert(html_recv_payload(&conn) == 0);
    }

    return 0;
}
