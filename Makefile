export PYTHONPATH=$(PWD)/../fabit
FAB=../fabit/fabricate

all: fabric
	$(FAB) mypi

deps:
	./make.py

fabric: ./make.py
	./make.py

clean:
	./make.py clean
	rm -f fabric

run: all
	./mypi
